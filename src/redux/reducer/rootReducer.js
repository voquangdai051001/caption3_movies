import { combineReducers } from "redux";
import UserReducer from "./userReducer";
import spinnerReducer from "./spinnerReducer";

export const rootReducer = combineReducers({ UserReducer, spinnerReducer });
