import { Progress, Space } from "antd";
export const progressMovie = {
  progressLine: (danhgia) => {
    return (
      <Progress
        percent={danhgia * 10}
        format={(percent) => `${percent / 10} điểm`}
        status="active"
        strokeColor={{
          from: "#d00000",
          to: "#00bbf9",
        }}
      />
    );
  },
  progressCircle: (danhgia) => {
    return (
      <Progress
        size={100}
        type="circle"
        percent={danhgia * 10}
        format={(percent) => `${percent / 10} điểm`}
        strokeColor={{
          "0%": "#d00000",
          "100%": "#00bbf9",
        }}
      />
    );
  },
};
