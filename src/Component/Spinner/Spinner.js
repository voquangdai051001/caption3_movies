import React from "react";
import { useSelector } from "react-redux";
import { PulseLoader } from "react-spinners";

export default function Spinner() {
  let { isLoading } = useSelector((state) => {
    return state.spinnerReducer;
  });

  return isLoading ? (
    <div className="h-screen w-screen bg-black flex items-center fixed top-0 left-0 z-50 justify-center">
      <PulseLoader color="#e76f51" size={100} speedMultiplier={1} />
    </div>
  ) : (
    <></>
  );
}
