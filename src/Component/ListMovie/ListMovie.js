import React, { useEffect, useState } from "react";
import { MovieServ } from "../../service/movieService";
import CardMovie from "./CardMovie";
import { useDispatch } from "react-redux";
import {
  batLoadingAction,
  tatLoadingAction,
} from "../../redux/action/spinnerAction";

export default function ListMovie() {
  const [movieList, setMovieList] = useState();
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(batLoadingAction());
    MovieServ.getMovie()
      .then((res) => {
        console.log(res);
        setMovieList(res.data.content);
        dispatch(tatLoadingAction());
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="container ">
      <div className="grid grid-cols-2 sm:grid-cols-3  lg:grid-cols-4 xl:grid-cols-4 gap-10 pt-10  mx-auto ">
        {movieList?.map((movie, index) => {
          return <CardMovie key={index} movie={movie} />;
        })}
      </div>
    </div>
  );
}
